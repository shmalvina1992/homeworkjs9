"use strict"

// 1. 
// const elem = document.createElement('div');
// elem.className = "название класса";
// elem.textContent = "текст внутри тега div"
// document.body.appendChild(elem);
// 2. Первый параметр функції insertAdjacentHTML является position. Варианты:
// <!-- beforebegin -->
// <p>
// <!-- afterbegin -->
// foo
// <!-- beforeend -->
// </p>
// <!-- afterend -->
// 3. elem.remove();

const inArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const newArr = ["Alexey", ["Alexander", "Sasha"], "Vitaly"];

function createList(inArr, parent = document.body) {
    const list = document.createElement('ul');
    parent.appendChild(list);

    inArr.forEach(element => {
        const listItem = document.createElement('li');

        if (!Array.isArray(element)) {
            listItem.textContent = element;
        } else {
            createList (element, listItem);
        } 

        list.appendChild(listItem);
    });
}

createList(inArr);
createList(newArr);